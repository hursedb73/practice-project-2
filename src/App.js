import "./App.scss";
import FormPage from "./formpage/FormPage";

function App() {
  return (
    <div className="App">
      <>
        <FormPage />
      </>
    </div>
  );
}

export default App;
