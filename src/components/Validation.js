function Validation(values) {
  let error = {};
  const email_pattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  const password_pattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{8,}$/;

  if (values.email === "") {
    error.email = "Укажите  E-mail";
  } else if (!email_pattern.test(values.email)) {
    error.email = "Неверный E-mail";
  }
  if (values.password === "") {
    error.password = "Укажите пароль.";
  } else if (!password_pattern.test(values.password)) {
    error.password = "Пароль не подходит";
  }

  if (values.confirm_password === "") {
    error.confirm_password = "Укажите пароль.";
  } else if (
    values.confirm_password === "" ||
    String(values.confirm_password) !== String(values.password)
  ) {
    error.confirm_password = "Пароли не совпадают";
  }

  return error;
}

export default Validation;
