import React from "react";

function Datepicker({ open }) {
  if (!open) return null;

  let showdate = new Date();
  let displaytodaydate =
    showdate.getDate() +
    "" +
    " февраля " +
    showdate.getFullYear() +
    " в " +
    showdate.getHours() +
    ":" +
    showdate.getMinutes() +
    ":" +
    showdate.getSeconds();
  return (
    <>
      <p
        style={{
          fontSize: "11px",
          fontFamily: "Arial",
          position: "relative",
          top: "695px",
          color: "#999999",
        }}
      >
        последние изменения {displaytodaydate}
      </p>
    </>
  );
}

export default Datepicker;
