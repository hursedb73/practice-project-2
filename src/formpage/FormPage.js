/* eslint-disable array-callback-return */
import React, { useState } from "react";
import "./FormPage.scss";
import data from "../data/Cities.json";
import Modal from "../modal/Modal";
import validation from "../components/Validation";
import Datepicker from "../components/Datepicker";

function FormPage() {
  let adults = data.filter((data) => {
    if (data.population >= 50000) {
      return true;
    }
  });
  // console.log(adults);

  let result = adults.sort(function (a, b) {
    if (a < b) return -1;
  });
  // console.log(result);

  const [openModal, setOpenModal] = useState(false);
  const [output, setOutput] = useState("Человек №3596941");

  const handleNameChange = (output) => {
    setOutput(output);
  };

  const [values, setValues] = useState({
    cities: "",
    resul: "",
    email: "",
    password: "",
    confirm_password: "",
  });

  const [errors, setErrors] = useState({});

  const handleInput = (e) => {
    setValues((prev) => ({ ...prev, [e.target.name]: [e.target.value] }));
  };

  function handleValidation(e) {
    e.preventDefault();
    setErrors(validation(values));
  }

  console.log(JSON.parse(JSON.stringify(values)));

  const [openDate, setOpenDate] = useState(false);

  return (
    <>
      <div className="container">
        <form onSubmit={handleValidation}>
          <h5 className="header">
            Здравствуйте, <b>{output}</b>
          </h5>
          <div className="status">
            <p
              onClick={() => {
                setOpenModal(true);
              }}
            >
              Сменить статус
            </p>
          </div>
          <div className="status-tooltip">
            <p>Прежде чем действовать, надо понять</p>
            <div className="group">
              <div className="rechtangle"></div>
              <div className="rechtangle2"></div>
            </div>
          </div>
          <div className="city">
            <input name="city" list="cities" style={{ paddingLeft: "10px" }} />
            <datalist id="cities">
              {result.map((data, i) => (
                <option key={i} value={data.city}></option>
              ))}
            </datalist>
            <div className="arrow"></div>
            <h4>Ваш город</h4>
            <hr />
          </div>
          <div className="password">
            <h4>Пароль</h4>
            <p>Ваш новый пароль должен содержать не менее 5 символов.</p>
            <input
              name="password"
              onChange={handleInput}
              value={values.password}
              type="password"
              style={{ textAlign: "left", paddingLeft: "10px" }}
            />
            {errors.password && (
              <p
                style={{
                  color: "red",
                  position: "absolute",
                  top: "65px",
                  left: "110px",
                }}
              >
                {errors.password}
              </p>
            )}
          </div>
          <div className="password-again">
            <h4>Пароль еще раз.</h4>
            <p>
              Повторите пароль, пожалуйста, это обезопасит вас с нами на случай
              ошибки.
            </p>
            <input
              name="confirm_password"
              onChange={handleInput}
              type="password"
              value={values.confirm_password}
              style={{ textAlign: "left", paddingLeft: "10px" }}
            />
            {errors.confirm_password && (
              <p
                style={{
                  color: "red",
                  position: "absolute",
                  top: "140px",
                  left: "225px",
                }}
              >
                {errors.confirm_password}
              </p>
            )}
            <hr />
          </div>
          <div className="email">
            <h4>Электронная почта</h4>
            <p>Можно изменить адрес, указанный при регистрации. </p>
            <input
              name="email"
              onChange={handleInput}
              value={values.email}
              type="email"
              style={{ textAlign: "left", paddingLeft: "10px" }}
            />
            {errors.email && (
              <p
                style={{
                  color: "red",
                  position: "absolute",
                  top: "65px",
                  left: "225px",
                }}
              >
                {errors.email}
              </p>
            )}
          </div>
          <div className="agree">
            <h4>Я сегласен</h4>
            <p>принимать актуальную информацию на емейл</p>
            <input type="checkbox" />
          </div>
          <div>
            <button className="redact" onClick={() => setOpenDate(true)}>
              <p>Изменить</p>
            </button>
            <Datepicker open={openDate} />
          </div>
        </form>
      </div>
      <Modal
        open={openModal}
        onClose={() => setOpenModal(false)}
        onChange={handleNameChange}
      />
    </>
  );
}
export default FormPage;
