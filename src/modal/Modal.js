import React from "react";
import "./Modal.scss";

function Modal({ open, onClose, onChange }) {
  if (!open) return null;

  let newReplace = React.createRef();

  let replace = () => {
    let click = newReplace.current.value;
    console.log(click);
  };

  const handleNameChange = (event) => {
    onChange(event.target.value);
  };

  return (
    <div className="overlay">
      <div className="modalContainer">
        <div className="modalRight">
          <p className="closeBtn" onClick={onClose}>
            ❌
          </p>
          <div className="content">
            <p>Введите новое значение</p>
            <input type="text" onChange={handleNameChange} />
          </div>
          <div className="btnContainer">
            <button className="btnPrimary" onClick={replace}>
              <span>Сохранить</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Modal;
